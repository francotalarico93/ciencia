//
//
//
//
//
//
//
//
//

(function(CienciaApp, $, undefined) {
    //
    //
    //
    window.CienciaApp.PlayerInstance = {
        //
        //
        //
        _timelineContainer: null,

        //
        //
        //
        _playButton: null,

        //
        //
        //
        _totalTime: 0,

        //
        //
        //
        _position: 0,

        //
        //
        //
        _audio: null,

        //
        //
        //
        _volumeLabelList: null,

        //
        //
        //
        _checkVolumeMutte: false,

        //
        //
        //
        _cuePointList: [],

        //
        //
        //
        _lastCuePointMargin: 0,

        //
        //
        //
        create: function(options) {
            //this._this = this;
            this._timelineContainer = $('#player-timeline');
            this._playButton = $('#player-playstop');
            this._volumeLabelList = $('#player-vol-container label');


            //
            //
            //
            this._playButton.on('click', this, this._onPlayStopClick);
            this._timelineContainer.on('click', this, this.onTimelineClick);

            //
            //
            //
            this._cuePointList = options["cuepoints"];

            //
            //
            //
            this._audio = new Audio(options["url"]);
            
            //this._audio.loadeddata = alert("w");
            $(this._audio).on('loadedmetadata', this, this._onLoadedMetadata);
            $(this._audio).on('timeupdate', this, this._onAudioTimeUpdate);
            $(this._audio).on('progress', this, function(e) { 
                if (this.buffered.length > 0 )
                    //e.data._timelineContainer.css("background-image", "-webkit-linear-gradient(left, #D5DB2F " + (this.buffered.end(0) / this.duration) * 100.0 + "%, #A4A2A7 0%)");
                    //console.log(this.buffered.end(0) / this.duration);
                    console.log(this.buffered.end(0))
            });
            //this._audio.ondurationchange = alert("w");
            this._audio.preload = true;

           // console.log( this._audio.duration );


           $('#player-vol-container input').on('change', this, this._onVolumeClick);
           $('label[for="player-vol-1"]').on('click', this, function(e) {
                //
                //
                var checked = $('#player-vol-1').is(':checked');

//                console.log( checked );

                //
                //
                //
                if (checked) {
                    e.data._checkVolumeMutte = true;
                    $('#player-vol-mutte').prop('checked', true);
                };
            });
            
        },

        //
        //
        //
        createTimeTrack: function(title, time) {

            //
            // Build the HTML.
            //
            var html = '<li data-time="'+ time +'"><span class="mark-title">' + title + '</span>' +
                       '<span class="mark-time">(' + this.formatTime(time) + ')</span><div class="mark-line"></div></li>';

            //
            //
            //
            var el = $(html).appendTo(this._timelineContainer);

            el.on('click', this, this._onTimeTrackClick);


            //
            //
            //
            var timePosition = time / this._totalTime;
            var linePosition = timePosition * this._timelineContainer.width() - 8;            

            $(el).css("margin-left", (linePosition - this._lastCuePointMargin) + "px");

            this._lastCuePointMargin = linePosition + 16;


            //
            //
            //
            var margin = $(el).find("span.mark-title").width() - $(el).find("span.mark-time").width();
            
            $(el).find("span.mark-time").css("margin-left", margin + "px");

        },

        //
        //
        //
        setPlayerVol: function(percent) {
            if (this._audio)
                this._audio.volume = percent;
        },

        //
        //
        //
        formatTime: function(time) {
            var h = Math.floor(time / 3600);
            var m = Math.floor((time - (h * 3600)) / 60);
            var s = time - (h * 3600) - (m * 60);

            if (h < 10) { h = '0' + h; }
            if (m < 10) { m = '0' + m; }
            if (s < 10) { s = '0' + s; }
            
            return h + ':' + m + ':' + s;
        },


        //
        //
        //
        _updateTimeline: function(time) {
            //this._totalTime
            /*
                                    pos = x - offset.left;
                        percentage = (pos / width);
                        newTime = (percentage <= 0.02) ? 0 : percentage * media.duration;

            */
            //var pos = this._position;

            //
            //
            //
            var p = (time / this._totalTime) * 100;

            //
            // We update the background gradient to the new position.
            //
            this._timelineContainer.css("background-image", "-webkit-linear-gradient(left, #D5DB2F " + p + "%, #A4A2A7 0%)");
        },

        //
        //
        //
        _togglePlayPause: function() {
            if (this._playButton.is(':checked') && this._audio.paused)
                this._audio.play();
            else
                this._audio.pause();
        },

        //
        //
        //
        onTimelineClick: function(e) {
            //var p = (e.offsetX / $(this).width()) * 100.0;
            //$(this).css("background-image", "-webkit-linear-gradient(left, #D5DB2F " + p + "%, #A4A2A7 0%)");

            var _this = e.data;

            //
            //
            //
            var percent = e.offsetX / _this._timelineContainer.width();
            var t =  (percent <= 0.02) ? 0 : percent * _this._totalTime;

            //_this._updateTimeline(t);
            e.data._audio.currentTime = t;

            //
            //
            //
            //_this._audio.play();
        },

        //
        //
        //
        _onTimeTrackClick: function(e) {
            e.stopPropagation();
            e.data._audio.currentTime = parseInt($(this).attr('data-time'));
            //e.data._updateTimeline(parseInt($(this).attr('data-time')));
        },

        //
        _onPlayStopClick: function(e) {
            var player = e.data;

            //
            player._togglePlayPause();
        },

        //
        //
        //
        _onVolumeClick: function(e) {
            //
            //
            //
            if ($(this).attr('id') == 'player-vol-1' && e.data._checkVolumeMutte) {
                //e.stopImmediatePropagation();
                $('#player-vol-mutte').prop('checked', true);
                e.data._checkVolumeMutte = false;
            }

            //
            //
            //
            switch( $('#player-vol-container input[type="radio"]:checked').attr('id') ) {
                case 'player-vol-5':
                    e.data.setPlayerVol(1.0);
                    break;
                case 'player-vol-4':
                    e.data.setPlayerVol(0.8);
                    break;
                case 'player-vol-3':
                    e.data.setPlayerVol(0.6);
                    break;
                case 'player-vol-2':
                    e.data.setPlayerVol(0.4);
                    break;
                case 'player-vol-1':
                    e.data.setPlayerVol(0.2);
                    break;
                case 'player-vol-mutte':
                    e.data.setPlayerVol(0);
                    break;
            }
        },

        //
        //
        //
        _onLoadedMetadata: function(e) {
            var _this = e.data;

            //
            //
            //
            _this._totalTime = this.duration;


            for (var i = _this._cuePointList.length - 1; i >= 0; i--) {
                _this.createTimeTrack(_this._cuePointList[i]["title"], _this._cuePointList[i]["time"]);
            };

            
        },

        //
        _onAudioTimeUpdate: function(e) {
            var player = e.data;
            player._updateTimeline(player._audio.currentTime);

            var el = $('#player-timeline li[data-time="' + Math.round(player._audio.currentTime) + '"]');

            /*

            if (el.length > 0 )
            {
                $('#player-timeline li.active').removeClass('active');
                $(el).addClass('active');
            }*/
        },
    };


    //
    //
    //
    window.CienciaApp.Player = function(options) {
        //
        // If the options aren't undefined, create the player.
        //
        if (typeof(options) == 'object')
            this.PlayerInstance.create(options);

        //
        return this.PlayerInstance;
    }
}( window.CienciaApp = window.CienciaApp || {}, jQuery ));

