<?php session_start();

//
//
include "rain.tpl.class.php";

//
raintpl::configure("path_replace", false);
raintpl::configure("tpl_dir", "tpl/" );
raintpl::configure("cache_dir", "tpl_cache/" );


if ( !empty($_POST) ) {
	//var_dump($_POST);

	switch ($_POST["form"]) {
		case "login":
			do_login($_POST["username"], $_POST["password"]);
			break;
		case "editclase":
			db_edit_clase();
			break;
		case "addclase":
			db_add_clase();
			break;
		default:
			var_dump($_POST); exit;
			break;
	}
}


//
//
//
check_login();



//
//
//
$_GET['page'] = (isset($_GET['page'])) ? $_GET['page'] : "home";



switch ($_GET['page']) {
	case 'logout':
		do_logout();
		break;
	case 'edit':
		show_edit($_GET["action"], $_GET["id"]);
		break;
	case 'add':
		show_add($_GET["action"]);
		break;
	case 'delete':
		do_delete_item($_GET["id"]);
	default:
		show_home();
		break;
}







function show_home()
{
	$mysqli = new mysqli("localhost", "clasepub_usrdata", "5JMcLs9OHcCTgeQrPV", "clasepub_data") or exit("Hubo un error al conectarse con la base de datos.");	
	$clases = array();

/*

*/

  	//
  	//
  	//
  	if ($result = $mysqli->query("SELECT * FROM program_list")) {
  		while ($row = $result->fetch_array(MYSQL_ASSOC)) {

  			//
  			//
  			//
			foreach ($row as $key => $value) {
  				if(is_string($value))
  					$row[$key] = stripslashes($value);
  			}
  			$row["date"] = date("d/m/Y", strtotime($row["date"]));

  			$clases[] = $row;
  		}

  		//
  		$result->close();
  	}
	$tpl = new RainTPL;

	$tpl->assign("username", $_SESSION["username"] );
	$tpl->assign("clases", $clases);
	$tpl->draw("home");
}


function show_edit($action, $id)
{
	switch ($action) {
		case 'clase':
			show_edit_class($id);
			break;
		
		default:
			# code...
			break;
	}
}


function show_edit_class($id)
{
	$tpl = new RainTPL;

	//$keywords

	$mysqli = new mysqli("localhost", "clasepub_usrdata", "5JMcLs9OHcCTgeQrPV", "clasepub_data") or exit("Hubo un error al conectarse con la base de datos.");	
	

	//
	//
	//
	if($stmt = $mysqli->prepare("SELECT * FROM program_list WHERE id=?")) {
		$stmt->bind_param("i", $id);

    	$stmt->execute();

		$result = mysqli_stmt_fetch_array($stmt);

        $result[0]["keywords"] = str_replace(" ", ",", $result[0]["keywords"]);

        $result[0]["date"] = date("d/m/Y", strtotime($result[0]["date"]));

        foreach ($result[0] as $key => $value) {
        	$result[0][$key] = htmlspecialchars(stripslashes($value), ENT_QUOTES, 'UTF-8');
        }

        $tpl->assign("program_data", $result[0]);


    	//$stmt->fetch();
    	$stmt->close();
  	} else {
    	exit("error: " . $mysqli->error);
  	}
  	$tpl->assign("username", $_SESSION["username"] );
	$tpl->assign("form_id", "editclase");
	$tpl->assign("clase_id", $id);
	$tpl->draw("edit-clase");
}


function db_edit_clase()
{
	$mysqli = new mysqli("localhost", "clasepub_usrdata", "5JMcLs9OHcCTgeQrPV", "clasepub_data") or exit("Hubo un error al conectarse con la base de datos.");

	$mysql_date = date('Y-m-d',  strtotime($_POST["datepicker"]));
	$keywords = str_replace(",", " ", $_POST["keywords"]);

	$imagePath = ($_FILES["image"]["error"] > 0) ? $_POST["_image_path"] : uploadImage($_FILES["image"]);

	foreach ($_POST as $key => $value) {
    	$_POST[$key] = htmlspecialchars_decode($value, ENT_QUOTES);
    }


	//
	//
	//
	if($stmt = $mysqli->prepare('UPDATE program_list SET date=?, author=?, title=?, info=?, image_path=?, audio_mp3=?, audio_ogg=?, keywords=?, description=? WHERE id=?')) {
    	$stmt->bind_param('sssssssssi', $mysql_date, $_POST["author"], $_POST["title"], $_POST["info"], $imagePath, 
    		$_POST["audio_mp3"], $_POST["audio_ogg"], $keywords, $_POST["description"], $_POST["clase_id"]);
    	$stmt->execute();
    	$stmt->close();

    	generate_json($mysqli);
    	$mysqli->close();

    	show_message("Modificar clase", "La clase se modifico correctamente.");
  	} else {
  		show_message("Modificar clase", "Error: " . $mysqli->error);
  	}
}


function show_add($action)
{
	switch ($action) {
		case 'clase':
			show_add_clase();
			break;
		
		default:
			# code...
			break;
	}
}


function show_add_clase()
{
	$tpl = new RainTPL;
	$tpl->assign("username", $_SESSION["username"] );
	$tpl->assign("form_id", "addclase");
	//$tpl->assign("clase_id", $id);
	$tpl->draw("edit-clase");
}

function db_add_clase()
{
	$mysqli = new mysqli("localhost", "clasepub_usrdata", "5JMcLs9OHcCTgeQrPV", "clasepub_data") or exit("Hubo un error al conectarse con la base de datos.");


	$mysql_date = date('Y-m-d',  strtotime($_POST["datepicker"]));
	$_POST["keywords"] = str_replace(",", " ", $_POST["keywords"]);
	$imageFile = uploadImage($_FILES["image"]);

	foreach ($_POST as $key => $value) {
    	$_POST[$key] = htmlspecialchars_decode($value, ENT_QUOTES);
    }


	//
	// NULL ,  '2013-06-12',  'test',  'test',  'test',  '',  '',  '',  '',  ''
	//
	if($stmt = $mysqli->prepare('INSERT INTO program_list VALUES (NULL,?,?,?,?,?,?,?,?,?)')) {
    	$stmt->bind_param('sssssssss', $mysql_date, $_POST["author"], $_POST["title"], $_POST["info"], $imageFile, 
    		$_POST["audio_mp3"], $_POST["audio_ogg"], $_POST["keywords"], $_POST["description"]);
    	$stmt->execute();
    	$stmt->close();

    	generate_json($mysqli);
    	$mysqli->close();
    	show_message("Agregar clase", "La clase se agrego correctamente.");
  	} else {
  		show_message("Agregar clase", "Error: " . $mysqli->error);
  	}
}


function show_message($title, $message)
{
	$tpl = new RainTPL;
	$tpl->assign("username", $_SESSION["username"] );
	$tpl->assign("title", $title);
	$tpl->assign("message", $message);
	$tpl->draw("message");
	exit;
}



function check_login()
{
	if (isset($_SESSION["auth_code"]) && isset($_SESSION["username"])) {
		$mysqli = new mysqli("localhost", "clasepub_usrdata", "5JMcLs9OHcCTgeQrPV", "clasepub_data") or exit("Hubo un error al conectarse con la base de datos.");	
	

		//
		//
		//
		if($stmt = $mysqli->prepare("SELECT username, password FROM users WHERE username=?")) {
			$stmt->bind_param("s", $_SESSION["username"]);

	    	$stmt->execute();

	    	$stmt->bind_result($result_user, $result_pw);
	    	$stmt->fetch();
	    	$stmt->close();

	    	//
	    	//
	    	//
	    	if ($_SESSION["auth_code"] == sha1($result_user.$result_pw)) {
	    		return;
	    	}
	  	} else {
	    	exit("error: " . $mysqli->error);
	  	}
	}

	$tpl = new RainTPL;
	$tpl->assign( "user", "test" );
	$tpl->draw("login");
	exit;
}


function do_login($user, $pass)
{
	$salted_pass = sha1("·!#clasepub_". $pass);


	$mysqli = new mysqli("localhost", "clasepub_usrdata", "5JMcLs9OHcCTgeQrPV", "clasepub_data") or exit("Hubo un error al conectarse con la base de datos.");	
	

	//
	//
	//
	if($stmt = $mysqli->prepare("SELECT username FROM users WHERE username=? AND password=?")) {
		$stmt->bind_param("ss", $user, $salted_pass);

    	$stmt->execute();

    	$stmt->bind_result($result);
    	$stmt->fetch();
    	$stmt->close();

    	if ($result == $user)
    	{
    		$_SESSION["username"] = $user;
    		$_SESSION["auth_code"] = sha1($user.$salted_pass);

    		header("Location: ./");
    	}
  	} else {
    	exit("error: " . $mysqli->error);
  	}
  	return false;
}

function do_logout()
{
	$_SESSION = array();
	session_destroy();
	header("Location: ./");
}


function do_delete_item($id)
{
	$mysqli = new mysqli("localhost", "clasepub_usrdata", "5JMcLs9OHcCTgeQrPV", "clasepub_data") or exit("Hubo un error al conectarse con la base de datos.");	
	

	//
	//
	//
	if($stmt = $mysqli->prepare("DELETE FROM program_list WHERE id=?")) {
		$stmt->bind_param("i", $id);

    	$stmt->execute();
    	$stmt->close();
    	generate_json($mysqli);
    	$mysqli->close();
    	show_message("Eliminar clase", "La clase se ha eliminado correctamente.");
  	} else {
    	exit("error: " . $mysqli->error);
  	}
}

function uploadImage($file) {
	$allowedExt = array("jpeg", "jpg", "png");
	$allowedMime = array("image/jpeg", "image/jpg", "image/x-png", "image/png");
	$fileExtension = end(explode(".", $file["name"]));

	//
	// check the mime.
	//
	if( in_array($file["type"], $allowedMime) && in_array($fileExtension, $allowedExt) ) {
		$hashedName = strtoupper(md5(mt_rand() . $file["name"])) . "." . $fileExtension;

		if ($file["error"] > 0)
		{
			exit("Return Code: " . $file["error"] . "<br />");
		}
		else
		{
	    	if( file_exists($_SERVER['DOCUMENT_ROOT'] . "/assets/images/front/" . $hashedName))
	      	{
	      		exit($hashedName . " already exists.");
	      	}
	    	else
	      	{
	      		move_uploaded_file($file["tmp_name"], $_SERVER['DOCUMENT_ROOT'] . "/assets/images/front/" . $hashedName);
	      		
	      		///echo "Stored in: " . "assets/images/front/" . $hashedName;
	      		return "assets/images/front/" . $hashedName;
	      	}
    	}
	}
	else
	{
		show_message("Error", "Archivo no valido.");
	}
}

function generate_json($mysqli)
{
	$json = array();

  	//
  	//
  	//
  	if ($result = $mysqli->query("SELECT * FROM program_list")) {
  		while($row = $result->fetch_array(MYSQL_ASSOC)) {

  			foreach ($row as $key => $value) {
  				if(is_string($value))
  					$row[$key] = stripslashes($value);
  			}

  			$json[] = $row;
  		}

  		//
  		$result->close();
  	}
	$fp = fopen($_SERVER['DOCUMENT_ROOT'] . "/assets/json/program_list.json", 'w');
	fwrite($fp, json_encode($json));
	fclose($fp);
}



function mysqli_stmt_fetch_array(&$stmt)
{
   $parameters = array();
   $results = array();

   $meta = $stmt->result_metadata();

   while ( $field = $meta->fetch_field() ) {

     $parameters[] = &$row[$field->name]; 
   }

   call_user_func_array(array($stmt, 'bind_result'), $parameters);

   while ( $stmt->fetch() ) {
      $x = array();
      foreach( $row as $key => $val ) {
         $x[$key] = $val;
      }
      $results[] = $x;
   }

   return $results;
}

?>